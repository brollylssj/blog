from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from django.utils import timezone
from taggit.managers import TaggableManager

from blog.constant import POST_STATUS_CHOICES, POST_CHOICE_DRAFT
from blog.managers import PostManager


class Post(models.Model):
    class Meta:
        ordering = ('-publish_date',)

    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, unique_for_date='publish_date')
    author = models.ForeignKey(User, related_name='blog_posts')  # related_name nazwa odwrotnego zwiazku
    body = models.TextField()
    publish_date = models.DateTimeField(default=timezone.now)
    created_date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=20, choices=POST_STATUS_CHOICES, default=POST_CHOICE_DRAFT)
    tags = TaggableManager()

    object = PostManager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:post_detail', args=[self.publish_date.year, self.publish_date.strftime('%m'),
                                                 self.publish_date.strftime('%d'), self.slug])


class Comment(models.Model):
    post = models.ForeignKey(Post, related_name='comments')
    name = models.CharField(max_length=80)
    email = models.EmailField()
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ('created',)

    def __str__(self):
        return 'Komentarz dodany przez {} dla posta {}'.format(self.name, self.post)