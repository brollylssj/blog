from django.db import models

from blog.constant import POST_CHOICE_PUBLISHED


class PostQuerySet(models.query.QuerySet):
    def __init__(self, *args, **kwargs):
        super(PostQuerySet, self).__init__(*args, **kwargs)

    def get_published_posts(self):
        return self.filter(status=POST_CHOICE_PUBLISHED)
