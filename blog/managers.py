from django.db import models

from blog.queryset import PostQuerySet


class PostManager(models.Manager):
    class GameManager(models.Manager):
        def get_query_set(self):
            return PostQuerySet(self.model, using=self._db)

    def get_published_posts(self):
        return self.get_queryset().get_published_posts()