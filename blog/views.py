from django.core.mail import send_mail
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Count
from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from django.views.generic.list import ListView
from taggit.models import Tag

from blog.constant import POST_CHOICE_PUBLISHED
from blog.forms import EmailPostForm, CommentForm
from blog.models import Post


class PostListView(ListView):
    model = Post
    template_name = 'blog/post/list.html'
    paginate_by = 3

    def get_context_data(self, **kwargs):
        tag_slug = self.kwargs.get('tag_slug', None)
        context = super(PostListView, self).get_context_data(**kwargs)
        object_list = Post.object.all()
        tag = None
        if tag_slug:
            tag = get_object_or_404(Tag, slug=tag_slug)
            object_list = object_list.filter(tags__in=[tag])
        paginator = Paginator(object_list, self.paginate_by)
        page = self.request.GET.get('page')
        try:
            posts = paginator.page(page)
        except PageNotAnInteger:
            posts = paginator.page(1)
        except EmptyPage:
            posts = paginator.page(paginator.num_pages)

        context['tag'] = tag
        context['posts'] = posts
        context['page'] = page
        return context


class PostDetailView(View):
    model = Post
    template_name = 'blog/post/detail.html'

    def get(self, request, year, month, day, post):
        comments, post = self._get_selected_comments_and_post(day, month, post, year)
        comment_form = CommentForm()
        post_tags_ids = post.tags.values_list('id', flat=True)
        similar_posts = Post.object.filter(tags__in=post_tags_ids).exclude(id=post.id)
        similar_posts = similar_posts.annotate(same_tags=Count('tags')).order_by('-same_tags', '-publish_date')[:4]
        return render(request, self.template_name, {'post': post, 'comments': comments, 'comment_form': comment_form,
                                                    'similar_posts': similar_posts})

    def post(self, request, year, month, day, post):
        comments, post = self._get_selected_comments_and_post(day, month, post, year)
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.post = post
            new_comment.save()
        return render(request, self.template_name, {'post': post, 'comments': comments, 'comment_form': comment_form})

    @staticmethod
    def _get_selected_comments_and_post(day, month, post, year):
        post = get_object_or_404(Post, slug=post, status=POST_CHOICE_PUBLISHED, publish_date__year=year,
                                 publish_date__month=month, publish_date__day=day)
        comments = post.comments.filter(active=True)
        return comments, post


class PostShareView(View):
    model = Post
    template_name = 'blog/post/share.html'

    def get(self, request, post_id):
        post = get_object_or_404(Post, id=post_id, status=POST_CHOICE_PUBLISHED)
        form = EmailPostForm()
        return render(request, self.template_name, {'post': post, 'form': form})

    def post(self, request, post_id):
        post = get_object_or_404(Post, id=post_id, status=POST_CHOICE_PUBLISHED)
        form = EmailPostForm(request.POST)
        sent = False
        if form.is_valid():
            email_data = form.cleaned_data
            post_url = request.build_absolute_uri(post.get_absolute_url())
            subject = '{} ({}) zacheca do przeczytania "{}"'.format(email_data['name'], email_data['email'],
                                                                    post.title)
            message = 'Przeczytaj post "{}" na stronie {} \n\n Komentarz dodany przez {}: {}'.format(post.title,
            post_url, email_data['name'], email_data['comments'])
            send_mail(subject=subject, message=message, from_email='admin@myblock.com',
                                 recipient_list=[email_data['to']])
            sent = True
        return render(request, self.template_name, {'post': post, 'form': form, 'sent': sent})
